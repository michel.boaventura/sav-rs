use time::{Tm, strptime};
use std::str;
use time;

pub fn buf_to_string(buffer: &[u8]) -> String {
    String::from_utf8(buffer.to_vec())
        .ok()
        .unwrap_or_else(String::new)
}

pub fn buf_to_tm(buffer: &[u8]) -> Tm {
    str::from_utf8(buffer)
        .ok()
        .and_then(|val| strptime(val, "%d %b %y%H:%M:%S").ok())
        .unwrap_or_else(time::now)
}
