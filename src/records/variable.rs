use nom::{le_i32, le_f64, IResult};
use utils::buf_to_string;

#[derive(Debug)]
pub struct Variable {
    rec_type: i32,
    var_type: i32,
    has_var_label: i32,
    n_missing_values: i32,
    print: i32,
    write: i32,
    name: String,

    // Present only if has_var_label is 1
    label_len: i32,
    label: String,

    // Present only if n_missing_values is nonzero
    missing_values: Vec<f64>,
}

fn label_len_tag(input: &[u8], has_var_label: i32) -> IResult<&[u8], i32> {
    if has_var_label == 1 {
        map!(input, le_i32, |c| c)
    } else {
        value!(input, 0)
    }
}

named!(pub variable<&[u8], Variable>, do_parse!(
        rec_type: le_i32                                >>
        var_type: le_i32                                >>
        has_var_label: le_i32                           >>
        n_missing_values: le_i32                        >>
        print: le_i32                                   >>
        write: le_i32                                   >>
        name: take!(8)                                  >>
        label_len: apply!(label_len_tag, has_var_label) >>
        label: take!(label_len)  >>
        missing_values: count!(le_f64, n_missing_values as usize) >>
        (Variable {
            rec_type,
            var_type,
            has_var_label,
            n_missing_values,
            print,
            write,
            name: buf_to_string(name),
            label_len,
            label: buf_to_string(label),
            missing_values,
        })
));

#[test]
fn variable_parser() {
    let v = include_bytes!("../../tests/fixtures/variable.sav");
    let variable = variable(v).to_result().unwrap();

    assert_eq!(variable.rec_type, 2);
    assert_eq!(variable.var_type, 0);
    assert_eq!(variable.has_var_label, 0);
    assert_eq!(variable.n_missing_values, 0);
    assert_eq!(variable.print, 328960);
    assert_eq!(variable.write, 328960);
    assert_eq!(variable.name, "ANO_CENS");
    assert_eq!(variable.label_len, 0);
    assert_eq!(variable.label, "");
    assert_eq!(variable.missing_values, vec![]);
}
