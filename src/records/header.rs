use nom::{le_i32, le_f64};
use time::Tm;
use utils::{buf_to_string, buf_to_tm};

#[derive(Debug)]
pub struct Header {
    rec_type: String,
    prod_name: String,
    layout_code: i32,
    nominal_case_size: i32,
    compression: i32,
    weight_index: i32,
    ncases: i32,
    bias: f64,
    creation: Tm,
    file_label: String,
}

named!(pub header<&[u8], Header>, do_parse!(
        rec_type:  take!(4)       >>
        prod_name: take!(60)      >>
        layout_code: le_i32       >>
        nominal_case_size: le_i32 >>
        compression: le_i32       >>
        weight_index: le_i32      >>
        ncases: le_i32            >>
        bias: le_f64              >>
        creation: take!(17)       >>
        file_label: take!(64)     >>
        take!(3) /* Padding */    >>
        (Header {
            rec_type: buf_to_string(rec_type),
            prod_name: buf_to_string(prod_name),
            layout_code,
            nominal_case_size,
            compression,
            weight_index,
            ncases,
            bias,
            creation: buf_to_tm(creation),
            file_label: buf_to_string(file_label),
        })
));

#[test]
fn header_parser() {
    let h = include_bytes!("../../tests/fixtures/header.sav");
    let header = header(h).to_result().unwrap();

    assert_eq!(header.rec_type, "$FL2");
    assert_eq!(header.prod_name, "@(#) IBM SPSS STATISTICS DATA FILE 64-bit MS Windows 20.0.0 ");
    assert_eq!(header.layout_code, 2);
    assert_eq!(header.nominal_case_size, 73);
    assert_eq!(header.compression, 1);
    assert_eq!(header.weight_index, 0);
    assert_eq!(header.ncases, 54436318);
    assert_eq!(header.bias, 100.0);
    assert_eq!(header.creation, buf_to_tm("21 Jun 1618:13:51".as_bytes()));
    assert_eq!(header.file_label, String::from_utf8(vec![b' '; 64]).unwrap());
}
